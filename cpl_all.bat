@echo off

call cpl_one.bat itlX64-12.0 mpich2x64-1.3.2p1
call cpl_one.bat itlX64-12.0 mpich2x64

call cpl_one.bat itlX86-12.0 mpich2
call cpl_one.bat itlX86-12.0 mpich2-1.3.2p1
call cpl_one.bat itlX86-12.0 deinompi-2.0.1

call cpl_one.bat gccX86      deinompi-2.0.1

:end